# Contributing to the tool

## Contents

  * [Installing Git](#installing-git)
  * [Using the Git repository entirely online](#using-the-git-repository-entirely-online)
  * [Using the Git repository locally](#using-the-git-repository-locally)
  * [Quick start guide to Squiffy syntax](#quick-start-guide-to-squiffy-syntax)

## Installing Git

You can download an installer from the [Git website][git]. The site also has
[suggestions for GUIs], but the simple operations are often easier at the
command line.

You will probably have to do a bit of [setting up](setup), like telling Git your
name and email address to use in its logs. To avoid mucking about with passwords
you can [set up an SSH key][sshkey] that Git will use instead to talk to GitLab.
Alternatively, there are several options for allowing Git to [store your passwords][cred] somewhere.

[git]: https://git-scm.com/
[suggestions for GUIs]: https://git-scm.com/downloads/guis
[setup]: https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup
[sshkey]: https://gitlab.com/help/gitlab-basics/create-your-ssh-keys.md
[cred]: https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage

## Using the Git repository entirely online

If you can't install Git locally, you can still contribute. To edit
`gw4-rdm-triage.squiffy`:

 1. [View the latest version][rdmts] of it on GitLab.

 2. If you only want to make a small change, select the 'Edit' button and edit
    the file in your browser. For larger edits, it might be better to do them
    locally:

     1. Make a note of when the file was last edited.
     2. Select the 'Raw' button and save the file on your computer.
     3. Do a chunk of work on it.
     4. [Reload and check the GitLab page][rdmts] to see if it has been updated.
          * If it hasn't, select the 'Replace' button to upload your local file
            over the top of it.
          * If it has, select the 'Edit' button and copy (only) your changes into
            it.

 3. Change the commit message to something more meaningful. The first line
    should describe the change in 72 characters or fewer and be written like an
    instruction. You can add more details on later lines if you want.

 4. Commit the changes.

This is great for quick edits, but can be cumbersome for larger ones, so if you
can install Git locally, it's better to do that instead.

[rdmts]: https://gitlab.com/gw4-rdm/rdm-triage/blob/master/gw4-rdm-triage.squiffy

## Using the Git repository locally

Pick or create a handy folder for keeping the project folder in, e.g.
`Projects`. Open up a command line prompt at that location and run this:

~~~
git clone https://gitlab.com/gw4-rdm/rdm-triage.git
~~~

If you haven't set up an SSH key, you will need your GitLab username and
password as this is a private repository. This will create a `rdm-triage` folder
in the current folder (e.g. `Projects`) containing the latest version of the
files.

Git allows you to have parallel versions or *branches* of a repository. For now
we'll keep things simple with a single branch, called *master*.

### Starting work

Before starting a session of work on the tool, pull in any recent changes. You
can do this at the command line by running the following while in the
`rdm-triage` folder:

~~~
git pull
~~~

### Doing work

Git works best when you do your work in small, contained chunks. As you edit the
files, stop and save each time you have done a chunk of work you could easily
describe, e.g. a section explaining data management plans.

Assuming you have changed `gw4-rdm-triage.squiffy`, run it through Squiffy just to
make sure it still builds properly. Then you can *stage* and *commit* that work
in Git all in one go with:

~~~
git commit -a -m "Write a section explaining data management plans"
~~~

The *commit message* should be

  - 72 characters or fewer
  - written like an instruction

If you want to explain your work in more detail you can do that. Leave off the
`-m "Write..."` bit and Git will open a text editor for you. Use the first line
for a short summary (as before) but then you can leave a blank line and write a
fuller description after that. Save the message and close the editor to
continue.

If you have changed several files but only want to commit one of them, you can
do that with a pair of commands; specify the file with the first, and the
commit message with the second:

~~~
git add gw4-rdm-triage.squiffy
git commit -m "Write a section..."
~~~

### Finishing work

When finishing a session of work, first see if anyone else has got there first:

~~~
git fetch
git status
~~~

If the status says your copy is *ahead* (but not behind) the server
(`origin/master`), simply push your changes upstream so others can see them:

~~~
git push
~~~

You will need to authenticate again, but Git will take care of this if you have
set up an [SSH key][sshkey] or [stored your credentials][cred].

* * *

If the status says your copy is both *ahead and behind* the server, you will
need to do some extra work. You have a choice. You can either do a *merge*,
where you keep both tracks of work separate and create a new commit to tie them
together:

~~~
git merge
~~~

or you can pretend you did all your work after the other person, which is called
*rebasing*:

~~~
git rebase
~~~

If you are lucky enough to have changed a different part of the file from the
other person, this will go through smoothly. You will now only be *ahead* of the
server, so you can push your changes upstream to complete the job:

~~~
git push
~~~

* * *

If you are unlucky, there will be a conflict, and you will get an error message.
Git will put both sets of changes in the file, marked like this:

~~~
<<<<<<< HEAD:gw4-rdm-triage.squiffy
Your changes
=======
Their changes
>>>>>>> origin/HEAD:gw4-rdm-triage.squiffy
~~~

For each one, replace the whole bit between and including the two lines of angle
brackets with what you want the end result to be.

If you are *merging*, simply stage and commit the changes as normal with a
message like "Merge changes to [whatever]":

~~~
git commit -a -m "Merge changes to section explaining data management plans"
~~~

If you are *rebasing*, run the following command to tell Git to fold your change
into whatever commit caused the problem:

~~~
git rebase --continue
~~~

Once all that is sorted out and you are only *ahead* of the upstream version,
upload your changes to complete the job:

~~~
git push
~~~

## Quick start guide to Squiffy syntax

Here is a quick start guide for editing .squiffy files. The [full
documentation][docs] is available from the Squiffy website. The syntax is based
on [Markdown][md], so you can use that for formatting your text.

[docs]: http://docs.textadventures.co.uk/squiffy/
[md]: https://daringfireball.net/projects/markdown/syntax

### Basic structure

The source is divided into **sections**. The first section (to be shown when the
page is first loaded) is unlabelled. All other sections (to be revealed through
user interaction) are labelled using double square brackets and a colon, like
this:

~~~
Initial text.

[[section 1]]:
This is section 1.

[[section 2]]:
This is section 2.
~~~

The order of the labelled sections doesn't matter.

A section may be divided into **passages**. A similar kind of rule applies: the
first passage is unlabelled and is shown straight away. Further passages may be
revealed later, and are labelled using single square brackets and a colon, like
this:

~~~
[[section 1]]:
Initial passage, or passage 0 if you like.

[passage 1]:
This is passage 1.

[passage 2]:
This is passage 2.
~~~

Passage labels only have to be unique within a section.

### Links

Users progress through the game by clicking links. There are different ways of
writing these links depending on what you want to happen.

  - To deactivate all current links on the page, and add new text to the end of
    the page, use a **section link**. The simplest way to do this is mark the
    link text with double square brackets, then use the same text to label a
    new section:

    ~~~
    [[section 1]]:
    This is section 1. Link to [[section 2]].

    [[section 2]]:
    This is section 2.
    ~~~

    To use a different (probably shorter) section label, include it in
    parentheses immediately after the link text:

    ~~~
    [[section 1]]:
    Link to [[the next section]](section 2).

    [[section 2]]:
    This is section 2.
    ~~~

  - To leave the other links active, and add new text to the end of the page,
    use a **passage link**. They work just the same but use only single square
    brackets:

    ~~~
    [[section 1]]:
    This is section 1. Link to [passage 1] and [longer passage link](passage 2).

    [passage 1]:
    This is passage 1.

    [passage 2]:
    This is passage 2.
    ~~~

    The text will appear in the order the links were selected, not the order
    they appear in the source.

  - To write a normal hyperlink, use a passage link but put the URL where the
    passage label would go:

    ~~~
    [Web link](http://example.com/)
    ~~~

    This **does not work** for email links: you have to write these out in full
    in HTML.

  - To use a link to change text somewhere else in the section, first label the
    text you want to change with `{label:lbl=}` (`lbl` can be anything):

    ~~~
    This is the {label:1=old text}.
    ~~~

    Then use a `@replace` command with a passage link to provide the replacement
    text, in any of the following ways:

    ~~~
    [Change it](@replace 1=new text)
    ~~~
    ~~~
    [Change it](@replace 1=change)

    [change]:
    new text
    ~~~
    ~~~
    [Change it](change)

    [change]:
    @replace 1=new text
    ~~~

  - To change the link text itself to something else, use **rotate** or
    **sequence**. These take a list of values that the link text can be, with
    each linking to the next. The difference is that with `{rotate:}`, the last
    value links back to the first, while with `{sequence:}` it doesn't. The
    values are separated by colons:

    ~~~
    Please choose: {rotate:red:green:blue}.
    Then, {sequence:ready...:steady...:go!}
    ~~~

    The last value in a `{sequence:}` can be a different kind of link.

### Variables

You can set a variable with a `@set` command on a line by itself:

~~~
@set inst = Exeter
~~~

Or you can use a link:

~~~
[[Exeter]](section 2, inst = Exeter)

Please choose: {rotate inst:Bath:Bristol:Cardiff:Exeter}
~~~

To display a variable, write it in braces:

~~~
Your university is {inst}.
~~~
