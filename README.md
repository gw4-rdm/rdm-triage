# Research Data Management Triage Tool

The purpose of this tool is to help postgraduate researchers with the
following types of query:

- Data management plans: what they are, what templates to use, what
  help is available.

- Storage options: institutional, non-institutional, how to request
  additional space, how to share data with collaborators.

- Organising files: devising a folder structure and file/folder naming
  convention.

- Documenting data: what to include, what format to use, what to put in
  a readme file.

- Archiving data: how to select data for retention, how to archive in a
  national or global data centre, how to archive locally.

As this tool is being written by and for GW4 members, the guidance it
offers is specific to the University of Bath, the University of Bristol,
Cardiff University and the University of Exeter.

## Compiling

The source code for the tool is in the .squiffy file. To compile into
a running game, you will need a recent copy of Squiffy:

  * There is an [online version][web] you can use. To be able to save
    changes online you will need to [register] for an account, but you
    can use the rest of the functionality as a guest.

  * You can download a [GUI version][gui] for Windows, Mac OS X or
    Linux. Depending on your platform you might be able to run it
    without installing it.

  * If you have Node.js installed, you can use the Node Package Manager
    to install a [command-line version][cli].

[web]: http://textadventures.co.uk/squiffy/editor
[register]: http://textadventures.co.uk/account/login?returnUrl=/squiffy/editor
[gui]: https://github.com/textadventures/squiffy-editor/releases
[cli]: http://docs.textadventures.co.uk/squiffy/cli.html

### Using the online version

 1. Copy the text of `gw4-rdm-triage.squiffy` into the left-hand pane.

 2. Test the code works as expected by selecting 'Preview'.

 3. Go back to the editor and select 'Download'.

    If compiling for the first time, select 'Export HTML and JavaScript'.
    You will receive the four files that comprise the tool:
    - `index.html`: The bare bones of the Web page that will run the
      tool.
    - `jquery.min.js`: Generic JavaScript for running the tool.
    - `story.js`: This contains the code for the tool itself.
    - `style.css`: This affects how the tool is formatted on screen.

    Most of these files will not be affected by subsequent edits, so
    to save time you can select 'Export JavaScript only' to get just
    the updated `story.js`.

Avoid selecting the 'Publish' button: we don't want to publish the tool
on the Text Adventures site!

### Using the GUI

 1. In Squiffy, open `gw4-rdm-triage.squiffy` in the usual way.

 2. Select 'Build'. This will generate the four files that comprise the
    tool (see above), and launch them in a browser for testing.

    They should appear in the same directory as the `gw4-rdm-triage.squiffy`
    file.

 3. To publish the tool, put the four generated files and the SVG files in an
    otherwise empty directory on a Web server.

## Further information

Please see the [Contribution guide](./CONTRIBUTING.md) for information on how
to edit the Triage Tool.

For dissemination activities and information on how the tool is hosted, please
see the tool's [Wiki pages](https://gitlab.com/gw4-rdm/rdm-triage/wikis/home).

If you wish to cite the tool academically, its development is described in this
paper:

> Alex Ball, Kellie Snow, Peter Obee and Greg Simpson (2017),
> ‘Choose Your Own Research Data Management Guidance’,
> *International Journal of Digital Curation* 12 (1): 13-21.
> <https://doi.org/10.2218/ijdc.v12i1.494>


## Licence

Copyright (C) 2018-2020  GW4 Open Research Working Group

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Pursuant to section 7b of the License, any derived versions must display
the copyright statement (as in the `div` element with `id` "copyright" in
`index.template.html`) with copyright information relating to modifications
appended.
