BASE=gw4-rdm-triage

.PHONY: clean

all: index.html jquery.min.js story.js style.css

story.js: $(BASE).squiffy
	squiffy $(BASE).squiffy --scriptonly

objects = index.html jquery.min.js style.css
index.html: index.template.html
style.css: style.template.css
$(objects):
	squiffy $(BASE).squiffy

clean:
	rm index.html jquery.min.js story.js style.css
